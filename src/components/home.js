import React from "react"
import styled from "styled-components"
import AOS from "aos"
import "aos/dist/aos.css"

const Container = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background: rgb(243, 244, 245);
`
const PageTitle = styled.div`
  font-weight: 700;
  font-size: 70px;
  color: #0575e6;
  text-align: center;
  margin: 0 20px;
  @media (max-width: 486px) {
    font-size: 40px;
  }
  @media (max-width: 362px) {
    font-size: 30px;
  }
`
export const HomePage = () => {
  React.useEffect(() => {
    AOS.init()
  }, [])
  return (
    <Container>
      <PageTitle>
        <span
          data-aos="fade-left"
          data-aos-delay="300"
          data-aos-duration="1500"
        >
          WELCOME TO{" "}
        </span>
        <span data-aos="fade-up" data-aos-delay="600" data-aos-duration="1500">
          {" "}
          ANGULAR SPA
        </span>
      </PageTitle>
    </Container>
  )
}
