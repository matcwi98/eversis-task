import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"
import LogoIcon from "../images/gear.png"

const StyledHeader = styled.header`
  position: fixed;
  width: 100%;
  height: 100px;
  display: flex;
  background: #021b79;
  color: #fff;
`
const StyledNav = styled.nav`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const StyledNavItem = styled.nav`
  margin: 0 40px;
`

const StyledLink = styled(Link)`
  text-decoration: none;
  color: #fff;
  font-weight: 600;
  font-size: 15px;
  letter-spacing: 1.2px;
  &:hover {
    color: rgb(223, 224, 225);
    letter-spacing: 1.4px;
  }
`
const Header = () => (
  <StyledHeader>
    <StyledNav>
      <StyledNavItem>
        <StyledLink to="/">
          <img src={LogoIcon} width="50px" height="50px" />{" "}
        </StyledLink>
      </StyledNavItem>{" "}
      <StyledNavItem>
        <StyledLink style={{ marginRight: "20px" }} to="/userData">
          Page1
        </StyledLink>
        <StyledLink to="/userPage">Page2</StyledLink>
      </StyledNavItem>
    </StyledNav>
  </StyledHeader>
)

export default Header
