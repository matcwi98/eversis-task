import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styled from "styled-components"
import Image from "../components/image"
import { navigateTo } from "gatsby"

const Container = styled.div`
  width: 100%;
  min-height: 100%;
  background: white;
  padding-top: 120px;
  color: #000;
`
const ContentWrapper = styled.div`
  display: flex;
  flex-flow: column wrap;
  justify-content: center;
  align-items: center;
  padding: 40px;
`

const UserDataDiv = styled.div`
  width: 100%;
  max-width: 600px;
  display: flex;
  flex-flow: row wrap;
  background: #8e2de2; /* fallback for old browsers */
  background: -webkit-linear-gradient(
    to right,
    #4a00e0,
    #8e2de2
  ); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(
    to right,
    #4a00e0,
    #8e2de2
  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

  color: #fff;
  justify-content: center;
  align-items: center;
  padding: 20px;

  box-shadow: 0 0.5rem 0.75rem -0.25rem rgba(39, 48, 54, 0.05);
`

const AccessButton = styled.a`
  width: 100%;
  max-width: 300px;
  background: #76ff03;
  padding: 20px;
  text-align: center;
  margin-top: 50px;
  margin-bottom: 50px;
  cursor: pointer;
  font-size: 18px;

  font-weight: 600;
  transition: all 0.5s;

  &:hover {
    border: 1px solid green;
  }
`
const ErrorContainer = styled.div`
  width: 100%;
  max-width: 300px;
  background: red;
  padding: 20px;
  text-align: center;
  margin-top: 50px;
  cursor: pointer;
`
const ImageBox = styled.div`
  width: 100%;
  max-width: 500px;
  height: 100%;
  max-height: 500px;
`

const UserPage = () => {
  let name =
    typeof window !== "undefined" ? localStorage.getItem("userName") : ""
  let surname =
    typeof window !== "undefined" ? localStorage.getItem("userSurname") : ""
  let age = typeof window !== "undefined" ? localStorage.getItem("userAge") : ""
  const [isImageVisibile, setIsImageVisibile] = React.useState(false)
  const [isErrorVisibile, setIsErrorVisibile] = React.useState(false)
  const handleAccessBtnClick = () => {
    if (age < 18) {
      setIsErrorVisibile(true)
    } else {
      setIsImageVisibile(true)
    }
  }
  return (
    <Layout>
      <SEO title="User Page" />
      <Container>
        <ContentWrapper>
          {name !== null && surname !== null && age !== null ? (
            <>
              <UserDataDiv>
                <span style={{ marginRight: "20px" }}>{name}</span>
                <span>{surname}</span>
              </UserDataDiv>
              <AccessButton onClick={handleAccessBtnClick}>Access</AccessButton>
              {isErrorVisibile && (
                <ErrorContainer> You by at least 18 years old !</ErrorContainer>
              )}
              {isImageVisibile && (
                <ImageBox>
                  <Image alt="success image" />
                </ImageBox>
              )}
            </>
          ) : (
            <>{navigateTo("/userData")}</>
          )}
        </ContentWrapper>
      </Container>
    </Layout>
  )
}

export default UserPage
