import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { HomePage } from "../components/home"

const IndexPage = () => (
  <Layout>
    <SEO title="Angular SPA" />
    <HomePage />
  </Layout>
)

export default IndexPage
