import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styled from "styled-components"
import { navigateTo } from "gatsby-link"

const Container = styled.div`
  width: 100%;
  min-height: 100vh;
  display: flex;
  background: rgb(243, 244, 245);
  padding-top: 120px;
`

const ContentWrapper = styled.div`
  width: 100%;
  height: 100%;
  padding: 40px;
  display: flex;
  flex-flow: column nowrap;

  @media (max-width: 1290px) {
    align-items: center;
  }
`

const TitleSection = styled.div`
  background: #0575e6; /* fallback for old browsers */
  background: -webkit-linear-gradient(
    to right,
    #021b79,
    #0575e6
  ); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(
    to right,
    #021b79,
    #0575e6
  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  font-size: 25px;
  font-weight: 600;
  color: #fff;
  text-decoration: none;
  padding: 20px;
  max-width: 700px;
  width: 100%;
  text-align: center;
  border-radius: 0.75rem;
  box-shadow: 0 0.5rem 0.75rem -0.25rem rgba(39, 48, 54, 0.05);
  margin: 20px 0 70px 0;
`
const DataRow = styled.div`
  display: flex;
  flex-flow: row wrap;
  margin-bottom: 50px;
  align-items: center;
  @media (max-width: 1290px) {
    flex-flow: column nowrap;
  }
`

const DataRowItem = styled.span`
  background: #0575e6; /* fallback for old browsers */
  background: -webkit-linear-gradient(
    to right,
    #021b79,
    #0575e6
  ); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(
    to right,
    #021b79,
    #0575e6
  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

  box-shadow: 0 0.5rem 0.75rem -0.25rem rgba(39, 48, 54, 0.05);
  border-radius: 0.75rem;
  font-size: 17px;
  font-weight: 400;
  color: #fff;
  text-decoration: none;
  padding: 20px;
  max-width: 300px;
  width: 100%;
  text-align: center;
  margin: 0px 100px 0 0;
  @media (max-width: 1290px) {
    margin-right: 0;
    margin-bottom: 20px;
  }
`
const DataRowItemError = styled(DataRowItem)`
  background: #ff4444;
`
const DataRowTextInput = styled.input`
  background: transparent;
  box-shadow: 0 0.5rem 0.75rem -0.25rem rgba(39, 48, 54, 0.05);
  border: 2px solid #0575e6;
  border-radius: 0.75rem;
  font-size: 17px;
  font-weight: 400;
  color: #000;
  text-decoration: none;
  padding: 20px;
  width: 100%;
  max-width: 300px;
  text-align: center;
  margin: 0px 100px 0 0;
  @media (max-width: 1290px) {
    margin-right: 0;
    margin-bottom: 20px;
  }
`
const Result = styled.span`
  width: 100%;
  max-width: 700px;
  background: #0575e6; /* fallback for old browsers */
  background: -webkit-linear-gradient(
    to right,
    #021b79,
    #0575e6
  ); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(
    to right,
    #021b79,
    #0575e6
  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  padding: 20px;
  color: #fff;
  margin-top: 10px;
  text-align: center;
  border-radius: 0.75rem;
  box-shadow: 0 0.5rem 0.75rem -0.25rem rgba(39, 48, 54, 0.05);
  font-size: 20px;
`

const SaveButton = styled.a`
  text-decoration: none;
  color: #8bc34a;
  letter-spacing: 1.2px;
  font-weight: 600;
  text-transform: uppercase;
  border: 3px solid #00c851;
  border-radius: 0.75rem;
  box-shadow: 0 0.5rem 0.75rem -0.25rem rgba(39, 48, 54, 0.05);
  background: white;
  max-width: 200px;
  width: 100%;
  text-align: center;
  padding: 20px;
  align-self: flex-start;
  margin-top: 10px;
  cursor: pointer;
  transition: all 0.5s;
  &:hover {
    background: #00c851;
    color: white;
  }
  @media (max-width: 1290px) {
    align-self: center;
  }
`
const SecondPage = () => {
  const [name, setName] = React.useState("")
  const [nameError, setNameError] = React.useState(false)
  const [surname, setSurname] = React.useState("")
  const [surnameError, setSurnameError] = React.useState(false)

  const [age, setAge] = React.useState("")
  const [ageError, setAgeError] = React.useState(false)

  const handleNameChange = event => {
    if (!/^[a-zA-Z]*$/.test(event.target.value)) {
      setNameError(true)
    } else {
      setNameError(false)
    }

    setName(event.target.value)
  }

  const handleSurnameChange = event => {
    if (!/^[a-zA-Z]*$/.test(event.target.value)) {
      setSurnameError(true)
    } else {
      setSurnameError(false)
    }
    setSurname(event.target.value)
  }
  const handleAgeChange = event => {
    if (!/^\d*$/.test(event.target.value)) {
      setAgeError(true)
    } else {
      setAgeError(false)
    }
    setAge(event.target.value)
  }

  const submitForm = () => {
    if (
      nameError ||
      surnameError ||
      ageError ||
      name.length < 1 ||
      surname.length < 1 ||
      age.length < 1
    ) {
      alert(
        "Please provide correct characters. Minimum 1 value in each input is required"
      )
    } else if (typeof window !== "undefined") {
      localStorage.setItem("userName", name)
      localStorage.setItem("userSurname", surname)
      localStorage.setItem("userAge", age)

      navigateTo("/userPage")
    }
  }
  return (
    <Layout>
      <SEO title="User Data" />
      <Container>
        <ContentWrapper>
          <TitleSection>User Data</TitleSection>
          <DataRow>
            <DataRowItem>Name:</DataRowItem>
            <DataRowTextInput type="text" onChange={handleNameChange} />
            {nameError && (
              <DataRowItemError>Invalid Character!</DataRowItemError>
            )}
          </DataRow>
          <DataRow>
            <DataRowItem>Surname:</DataRowItem>
            <DataRowTextInput type="text" onChange={handleSurnameChange} />
            {surnameError && (
              <DataRowItemError>Invalid Character!</DataRowItemError>
            )}
          </DataRow>
          <DataRow>
            <DataRowItem>Age:</DataRowItem>
            <DataRowTextInput type="text" onChange={handleAgeChange} />
            {ageError && (
              <DataRowItemError>Invalid Character!</DataRowItemError>
            )}
          </DataRow>
          <DataRow>
            <Result>
              Hello {name} {surname}
            </Result>
          </DataRow>
          <SaveButton onClick={submitForm}>Save</SaveButton>
        </ContentWrapper>
      </Container>
    </Layout>
  )
}

export default SecondPage
